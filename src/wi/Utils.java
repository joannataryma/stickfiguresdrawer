/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wi;

import javax.swing.Icon;
import javax.swing.JLabel;

/**
 *
 * @author Joasia
 */
public class Utils {
    public enum Patyczaki {
        
        HIGHER(MainWindow.higherIcon), 
        VOCATIONAL(MainWindow.vocationalIcon), 
        SECONDARY(MainWindow.secondaryIcon), 
        PRIMARY(MainWindow.primaryIcon), 
        NONE(MainWindow.noneIcon);
        
        private JLabel _label;
        
        private Patyczaki(JLabel label) {
            this._label = label;
        }
        
        public Icon getImage() {
            return this._label.getIcon();
        }
        
        public static String getFromInt(int index) {
            switch(index) {
                case 0: 
                    return "h";
                case 1:
                    return "v";
                case 2:
                    return "s";
                case 3:
                    return "p";
                case 4:
                    return "n";
                default:
                    return "n";
            }
        }
        public static Patyczaki getFromString(char ch) {
            switch(ch) {
                case 'h': 
                    return HIGHER;
                case 'v':
                    return VOCATIONAL;
                case 's':
                    return SECONDARY;
                case 'p':
                    return PRIMARY;
                case 'n':
                    return NONE;
                default:
                    return NONE;
            }
        }
    }
    
    public class Pair {
        private int x;
        private int y;

        public Pair(int x, int y) {
            this.x = x;
            this.y = y;
        }

        
        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }
}
